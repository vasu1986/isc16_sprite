library(VennDiagram)
draw.triple.venn(area1 = 3976238, area2 = 4036221, area3 = 4001886, n12 = 3772017, n23 = 3839822, n13 = 3833444, n123 = 3743375, category = c("SPRITE", "GATK-HC", "Speedseq"), lty = "blank", fill = c("skyblue", "pink1", "mediumorchid"), print.mode ="percent", cex=c(2,2,2,2,2,2,2), cat.cex=c(2,2,2), cat.pos=c(0,0,180))
dev.copy(pdf,"gatkSpeedseqSpriteVenn.pdf")
dev.off()
